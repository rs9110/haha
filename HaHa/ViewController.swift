//
//  ViewController.swift
//  HaHa
//
//  Created by Student on 9/19/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let HA_HA_FILE = "haha"
    let HA_HA_FORMAT = "mp3"
    
    var player: AVAudioPlayer?
    var counter: Int
    
    @IBOutlet weak var label: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        counter = 0
        
        //initialize audio player
        if let soundFilePath = Bundle.main.path(forResource: HA_HA_FILE, ofType: HA_HA_FORMAT) {
            let fileUrl = URL(fileURLWithPath: soundFilePath)
            
            do {
                player = try AVAudioPlayer(contentsOf: fileUrl)
            } catch {
                player = nil
                print(error)
            }
        } else {
            player = nil
        }
        player?.prepareToPlay() //eliminates delay later
        super.init(coder: aDecoder)
    }
    
    @IBAction func playSound(sender: UIButton) {
        if (player != nil) {
            player?.play()
            counter += 1
            label.text = "Ha Ha #\(counter)"
        }
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        //animate the button
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        shake.fromValue = NSValue(cgPoint: CGPoint(x: sender.center.x-5, y: sender.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: sender.center.x+5, y: sender.center.y))
        sender.layer.add(shake, forKey: "position")
    }
    
    //method to stop the audio
    @IBAction func stopSound(_ sender: UIButton) {
        if(player != nil) {
            player?.stop()
            counter = 0
            label.text = "Audio Stopped"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.shadowColor = UIColor.gray
        label.shadowOffset = CGSize(width: 3, height: -3)
        label.textColor = UIColor.white
        label.font = UIFont(name: "markerFelt-Wide", size: 30)
        label.text = ""
    }


}

